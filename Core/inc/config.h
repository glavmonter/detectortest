#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_


// Альтернативный измерительный таймер TIM21_CH2
// Только для доработанных плат
#ifndef TIM_ALT
#define TIM_ALT         1
#endif


// Режим измерения частоты: Два таймера или DMA
#define FREQ_MODE_DMA           1
#define FREQ_MODE_TWO_TIMER     2
#define FREQ_MODE_ONE_TIMER     3

#define FREQ_MODE               FREQ_MODE_TWO_TIMER



#define PLATFORM_EPCOS      1
#define PLATFORM_CHINA      2
#define HW_PLATFORM         PLATFORM_CHINA


#endif /* INC_CONFIG_H_ */
