#ifndef INC_HARDWARE_H_
#define INC_HARDWARE_H_

#include "config.h"

// Выбор светодиодной линейки
#define BAR_PORT            GPIOB
// BAR0 - PB0
#define BAR0_o_PIN          LL_GPIO_PIN_0
#define BAR0_o_PORT         GPIOB

// BAR1 - PB1
#define BAR1_o_PIN          LL_GPIO_PIN_1
#define BAR1_o_PORT         GPIOB

// BAR2 - PB2
#define BAR2_o_PIN          LL_GPIO_PIN_2
#define BAR2_o_PORT         GPIOB




// Выходы выбора частоты работы
#if HW_PLATFORM == PLATFORM_CHINA
// SEL0_C - PB14
#define SEL0_C_o_PIN        LL_GPIO_PIN_14
#define SEL0_C_o_PORT       GPIOB

// SEL1_B - PB15
#define SEL1_B_o_PIN        LL_GPIO_PIN_15
#define SEL1_B_o_PORT       GPIOB

// SEL2_A - PA8
#define SEL2_A_o_PIN        LL_GPIO_PIN_8
#define SEL2_A_o_PORT       GPIOA

#elif HW_PLATFORM == PLATFORM_EPCOS
// SEL0_C - PB13
#define SEL0_C_o_PIN        LL_GPIO_PIN_13
#define SEL0_C_o_PORT       GPIOB

// SEL1_B - PB14
#define SEL1_B_o_PIN        LL_GPIO_PIN_14
#define SEL1_B_o_PORT       GPIOB

// SEL2_A - PB15
#define SEL2_A_o_PIN        LL_GPIO_PIN_15
#define SEL2_A_o_PORT       GPIOB
#endif


// Выходы на транзисторы реле
#define RELAY_PORT          GPIOB
// RELAY0 - PB5
#define RELAY0_o_PIN        LL_GPIO_PIN_5
#define RELAY0_o_PORT       GPIOB

// RELAY1 - PB6
#define RELAY1_o_PIN        LL_GPIO_PIN_6
#define RELAY1_o_PORT       GPIOB

// RELAY2 - PB7
#define RELAY2_o_PIN        LL_GPIO_PIN_7
#define RELAY2_o_PORT       GPIOB


// Входы измерения переключателей
#define SW_PORT             GPIOB
// SW0 - PB11
#define SW0_i_PIN           LL_GPIO_PIN_11
#define SW0_i_PORT          GPIOB

// SW1 - PB10
#define SW1_i_PIN           LL_GPIO_PIN_10
#define SW1_i_PORT          GPIOB


#if HW_PLATFORM == PLATFORM_CHINA
// Контрольные точки
// CONTROL0 - PB12
#define CONTROL0_PIN        LL_GPIO_PIN_12
#define CONTROL0_PORT       GPIOB

// CONTROL1 - PB13
#define CONTROL1_PIN        LL_GPIO_PIN_13
#define CONTROL1_PORT       GPIOB

#elif HW_PLATFORM == PLATFORM_EPCOS
// Контрольные точки
// CONTROL0 - PA3
#define CONTROL0_PIN        LL_GPIO_PIN_3
#define CONTROL0_PORT       GPIOA

// CONTROL1 - PB12
#define CONTROL1_PIN        LL_GPIO_PIN_12
#define CONTROL1_PORT       GPIOB
#endif


// SPI Chip select
#define SPI1_CS_PIN         LL_GPIO_PIN_6
#define SPI1_CS_PORT        GPIOA

#define SPI1_SCK_PORT       GPIOA
#define SPI1_SCK_PIN        LL_GPIO_PIN_5

#define SPI1_MOSI_PORT      GPIOA
#define SPI1_MOSI_PIN       LL_GPIO_PIN_7
#define SPI1_GPIO_AF        LL_GPIO_AF_0
#define SPI1_GPIO_PORT      GPIOA


// TIM2 channels, pulse generator
#define TIM2_CH1_PIN        LL_GPIO_PIN_0
#define TIM2_CH2_PIN        LL_GPIO_PIN_1
#define TIM2_CH3_PIN        LL_GPIO_PIN_2
//#define TIM2_CH4_PIN        LL_GPIO_PIN_3
#define TIM2_PORT           GPIOA
#define TIM2_GPIO_AF        LL_GPIO_AF_2

#define LOOP_A_EN_PIN       TIM2_CH1_PIN
#define LOOP_B_EN_PIN       TIM2_CH2_PIN
#define LOOP_C_EN_PIN       TIM2_CH3_PIN
#define LOOP_ABC_EN_PORT    TIM2_PORT

// TIM22 ETR - PA4
#define TIM22_ETR_PIN       LL_GPIO_PIN_4
#define TIM22_ETR_PORT      GPIOA
#define TIM22_GPIO_AF       LL_GPIO_AF_5

#if (FREQ_MODE == FREQ_MODE_TWO_TIMER) or (FREQ_MODE == FREQ_MODE_ONE_TIMER)

#define TIM21_CH2_PIN       LL_GPIO_PIN_3
#define TIM21_CH2_PORT      GPIOA
#define TIM21_GPIO_AF       LL_GPIO_AF_0

#if FREQ_MODE == FREQ_MODE_TWO_TIMER
#define TIM21_CH1_PIN       LL_GPIO_PIN_2
#define TIM21_CH1_PORT      GPIOA
#endif
#endif


#if FREQ_MODE == FREQ_MODE_DMA
#define TEST_PIN            LL_GPIO_PIN_2
#define TEST_PIN_PORT       GPIOA
#endif

// USART1_TX - PA9
#define MODBUS_TX_PIN       LL_GPIO_PIN_9
#define MODBUS_TX_PORT      GPIOA
#define MODBUS_TX_GPIO_AF   LL_GPIO_AF_4

// USART1_RX - PA10
#define MODBUS_RX_PIN       LL_GPIO_PIN_10
#define MODBUS_RX_PORT      GPIOA
#define MODBUS_RX_GPIO_AF   LL_GPIO_AF_4

// USART1_DE - PA12
#define MODBUS_DE_PIN       LL_GPIO_PIN_12
#define MODBUS_DE_PORT      GPIOA
#define MODBUS_DE_GPIO_AF   LL_GPIO_AF_4

#define MODBUS_UART         USART1


#endif /* INC_HARDWARE_H_ */
