#ifndef INC_FILTER_HPP_
#define INC_FILTER_HPP_
#include "etl/algorithm.h"


template <typename Tinput, typename Toutput, const int order=2>
class Filter {
public:
    typedef Tinput output_type;

    Filter() {
    }

    Tinput sample(Tinput sample) {
        last_value = sample;
        return last_value;
    }

    Tinput value() {
        return last_value;
    }

private:
    Tinput last_value = 0;
};


namespace filter_none {
    using Filter = Filter<int32_t, int64_t, 2>;
}

#endif /* INC_FILTER_HPP_ */
