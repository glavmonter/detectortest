#pragma once

#include <cstdio>
#include "stm32l0xx.h"
#include "stm32l0xx_ll_gpio.h"
#include "hardware.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "etl/vector.h"
#include "etl/callback_timer.h"
#include "etl/type_traits.h"
#include "Leds.h"
#include "ring_buffer.h"
#include "common.h"
#include "filter.hpp"

#include "boost/sml.hpp"

namespace sml = boost::sml;

namespace lc {

#define RING_BUFFER_SIZE        256
using RB = RingBuffer<RING_BUFFER_SIZE, char>;

RB& GetRB();

#define SETTINGS_SENS_A_Pos             (14U)
#define SETTINGS_SENS_A_Msk             (0x3UL << SETTINGS_SENS_A_Pos)
#define SETTINGS_SENS_B_Pos             (12U)
#define SETTINGS_SENS_B_Msk             (0x3UL << SETTINGS_SENS_B_Pos)
#define SETTINGS_SENS_C_Pos             (10U)
#define SETTINGS_SENS_C_Msk             (0x3UL << SETTINGS_SENS_C_Pos)
#define SETTINGS_MODE_Pos               (8U)
#define SETTINGS_MODE_Msk               (0x3UL << SETTINGS_MODE_Pos)

#define SETTINGS_FREQ_A_Pos             (7U)
#define SETTINGS_FREQ_A_Msk             (0x1UL << SETTINGS_FREQ_A_Pos)
#define SETTINGS_FREQ_B_Pos             (6U)
#define SETTINGS_FREQ_B_Msk             (0x1UL << SETTINGS_FREQ_B_Pos)
#define SETTINGS_FREQ_C_Pos             (5U)
#define SETTINGS_FREQ_C_Msk             (0x1UL << SETTINGS_FREQ_C_Pos)


#define SETTINGS_ENABLE_A_Pos           (2U)
#define SETTINGS_ENABLE_A_Msk           (0x1UL << SETTINGS_ENABLE_A_Pos)
#define SETTINGS_ENABLE_B_Pos           (1U)
#define SETTINGS_ENABLE_B_Msk           (0x1UL << SETTINGS_ENABLE_B_Pos)
#define SETTINGS_ENABLE_C_Pos           (0U)
#define SETTINGS_ENABLE_C_Msk           (0x1UL << SETTINGS_ENABLE_C_Pos)


#define PRECALIBRATION_COUNT            8

constexpr TickType_t CalibrationTime = 8000;

typedef enum {
    LE_NO_ERROR = 0,
    LE_SHORT,
    LE_OPEN,
    LE_FATAL
} LoopError;

typedef enum {
    LOOP_A = 0,
    LOOP_B,
    LOOP_C,
    LOOP_END
} LoopName;


const struct {
    GPIO_TypeDef *port;
    uint32_t pin;
} relays[LOOP_END] = {
        {RELAY2_o_PORT, RELAY2_o_PIN}, // LOOP_A
        {RELAY1_o_PORT, RELAY1_o_PIN}, // LOOP_B
        {RELAY0_o_PORT, RELAY0_o_PIN}  // LOOP_C
};


typedef struct {
    LoopName loop;
    uint16_t value;
    uint16_t multiplier;
} Measure;


typedef struct {
    LoopName loop;
    uint16_t multiplier;
} Multiplier;


struct StartEv   {};

struct MeasureEv {
    LoopName loop;
    uint16_t value;
    uint16_t multiplier;
};


class LoopController;


template <typename T>
    struct CalibrationConsts {
        T CalibrationValue;
        T ThresholdLow;
        T ThresholdLowHyst;
        T lta_low;
        T lta_high;
        T lta_delta;

        void CalcCalibration(T cal, T sens) {
            auto delta = cal * sens / 10000;
            CalibrationValue = cal;
            ThresholdLow  = CalibrationValue - delta;
            ThresholdLowHyst = ThresholdLow + delta / (T)3;

            lta_delta = CalibrationValue * 64 / 10000;
            lta_low = CalibrationValue - lta_delta;
            lta_high = CalibrationValue + lta_delta;
        }

        char *PrintCalibration(char *buf) {
            if (etl::is_floating_point<T>::value) {
                char *s = _float_to_char(CalibrationValue, buf);
                return s;
            } else{
                sprintf(buf, "%d", (int)CalibrationValue);
                return buf;
            }
        }

        char *PrintThresholdLow(char *buf) {
            if (etl::is_floating_point<T>::value) {
                char *s = _float_to_char(ThresholdLow, buf);
                return s;
            } else{
                sprintf(buf, "%d", (int)ThresholdLow);
                return buf;
            }
        }

        char *PrintThresholdLowHyst(char *buf) {
            if (etl::is_floating_point<T>::value) {
                char *s = _float_to_char(ThresholdLowHyst, buf);
                return s;
            } else{
                sprintf(buf, "%d", (int)ThresholdLowHyst);
                return buf;
            }
        }
    };



namespace sample_filter = filter_none;
namespace lta_filter = filter_none;

class Loop {
    using Self = Loop;

public:
    explicit Loop(LoopController &lc) : lc(lc) {}

    void InitObjects(LoopName name, BarNumber bar) {
        _loop = name;
        _name = (char)name + 'A';
        _bar = bar;
    };

    void SetSensitivity(uint8_t sensitivity) {
    }

    void SetSelection(bool sel) {
        bSelection = sel;
    }


    auto operator()() const noexcept {
        using namespace sml;

        return make_transition_table(
               *"idle"_s         + event<StartEv>                             / &Self::StartReceived    = "precal"_s,
                "precal"_s       + event<MeasureEv> [&Self::PreCalibratingGuard] / &Self::PreCalDone    = "check pre"_s,
                "check pre"_s      [&Self::PreCalCheck]                                                 = "calibrate"_s,
                "calibrate"_s    + event<MeasureEv> [&Self::CalibratingGuard] / &Self::CalibratingDone  = "error"_s,
                "calibrate"_s    + sml::on_entry<_>                           / &Self::OnCalibratingEnter,
                "error"_s        + sml::on_entry<_>                           / &Self::ErrorEntry,
                "error"_s        + event<MeasureEv> [&Self::ErrorGuard]       / &Self::StartReceived    = X
        );
    }

private:
    LoopController &lc;
    sample_filter::Filter filter_sample {};


private:
    void StartReceived() const;
    bool CalibratingGuard(const MeasureEv &e);
    bool PreCalibratingGuard(const MeasureEv &e);

    void OnCalibratingEnter();
    void CalibratingDone();
    bool CalibrationError();

    void ErrorEntry();
    bool ErrorGuard();
    void PreCalDone();
    bool PreCalCheck();
    uint16_t tim_multiplier = 0;

    __attribute__((unused)) LoopError GetLoopError(float value);
    LoopError GetLoopError(int32_t value);

    char _name = 'A';
    LoopName _loop = LOOP_END;
    BarNumber _bar = BARS_MAX;

    int iErrorTimeout = 0;
    int iGuardCounter = 0;
    TickType_t iCalibrationEndTime = 0;

    etl::vector<uint16_t, PRECALIBRATION_COUNT> adata;
    CalibrationConsts<int32_t> Calibrations{};
    bool bSelection = false;
};


class LoopController {
public:
    LoopController() = default;
    void InitObjects();

    void task();
    static void task_lc(void *param) {
        static_cast<LoopController *>(param)->task();
        while (true)
            vTaskDelay(portMAX_DELAY);
    }

    QueueHandle_t xQueueSettings = nullptr;
    QueueHandle_t xQueueLoopMeasure = nullptr;
    QueueHandle_t xQueueMultiplier = nullptr;

    static void UartPrint(RB &rb, const char *str);

private:
    void StartReceived();
    static void SetFrequencyRange(uint16_t settings);
    // Работа со светодиодами
public:
    void SetRedBlink(BarNumber bar, uint32_t period);
    void SetRedState(BarNumber bar, bool state);
    void SetGreen(BarNumber bar, uint8_t state);
    void SetGreenLevel(BarNumber bar, uint8_t level);
    xQueueHandle xLedState = nullptr;

    void RedLedCallback(BarNumber bar);

    void TemperatureCallback();

private:
    etl::timer::id::type led_timers[BARS_MAX] = {};
    bool red_state[BARS_MAX] = {};
    uint8_t green_state[BARS_MAX] = {};
    void SendLedUpdate();

    etl::timer::id::type temperature_timer = 0;
};

}


lc::LoopController &getLC();
