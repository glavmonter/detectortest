#ifndef INC_LEDS_H_
#define INC_LEDS_H_
#include <FreeRTOS.h>
#include <semphr.h>
#include <timers.h>

#include "etl/bitset.h"


/*
 * Светодиоды на плате расположены не по номерам битов, а следуюшим образом:
 * Верх платы
 * Bit   Color
 *  0    g[6]
 *  1    g[5]
 *  2    g[4]
 *  3    g[3]
 *  7    g[2]
 *  6    g[1]
 *  5    g[0]
 *  4    red
 */


typedef enum {
    BAR_A = 0,
    BAR_B,
    BAR_C,
    BARS_MAX
} BarNumber;


class Leds {
public:
    Leds() {
        Normalize();
    }
    void set(const char *text) {
        leds.set(text);
        Normalize();
    }

    void Normalize() {
        // Перебросить выводы из виртуальных в реальные на плате индикатора
        //                HW  Virtual
        leds_normalize.set(4, leds[7]);
        leds_normalize.set(5, leds[0]);
        leds_normalize.set(6, leds[1]);
        leds_normalize.set(7, leds[2]);
        leds_normalize.set(3, leds[3]);
        leds_normalize.set(2, leds[4]);
        leds_normalize.set(1, leds[5]);
        leds_normalize.set(0, leds[6]);
        _value = ~leds_normalize.value<uint8_t>();
    }

    void set_red(bool val) {
        leds.set(7, val);
        Normalize();
    }

    void set_green(uint8_t val) {
        for (int i = 0; i < 7; i++) {
            leds.set(i, val & (1 << i));
        }
        Normalize();
    }

    void set_all(uint8_t val) {
        for (int i = 0; i < 8; i++) {
            leds.set(i, val & (1 << i));
        }
        Normalize();
    }

    uint8_t value() const {
        return _value;
    }

private:
    etl::bitset<8> leds;
    etl::bitset<8> leds_normalize;
    uint8_t _value = 0;
};

#endif /* INC_LEDS_H_ */
