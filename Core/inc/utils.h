#ifndef INC_UTILS_H_
#define INC_UTILS_H_
#include <cstdint>

namespace utils {
    uint8_t GetGreenLevel(uint8_t level);
}


#endif /* INC_UTILS_H_ */
