#include <string.h>
#include <cstdio>
#include "stm32l0xx_hal.h"
#include "SEGGER_RTT.h"
#include "LoopController.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_usart.h"
#include "hardware.h"
#include "common.h"
#include "etl/function.h"
#include "adc.h"
#include "utils.h"

#define SELFTEST    1
#define MODNAME "[Loop] "

SemaphoreHandle_t UartMutex;

namespace lc {

RB TransmitBuffer;
char PrintfBuffer[RING_BUFFER_SIZE];

RB& GetRB() {
    return TransmitBuffer;
}

LoopController ctrl;
Loop loopa(ctrl);
Loop loopb(ctrl);
Loop loopc(ctrl);
sml::sm<Loop> sm_loop_a{loopa};
sml::sm<Loop> sm_loop_b{loopb};
sml::sm<Loop> sm_loop_c{loopc};


static etl::callback_timer<4> timer_controller;
static etl_ext::function_mpval<LoopController, BarNumber, &LoopController::RedLedCallback> callback_a(ctrl, BAR_A);
static etl_ext::function_mpval<LoopController, BarNumber, &LoopController::RedLedCallback> callback_b(ctrl, BAR_B);
static etl_ext::function_mpval<LoopController, BarNumber, &LoopController::RedLedCallback> callback_c(ctrl, BAR_C);
static etl::function_imv<LoopController, ctrl, &LoopController::TemperatureCallback> temperature_callback;


void LoopController::task() {

    led_timers[BAR_A] = timer_controller.register_timer(callback_a, 100, etl::timer::mode::REPEATING);
    led_timers[BAR_B] = timer_controller.register_timer(callback_b, 100, etl::timer::mode::REPEATING);
    led_timers[BAR_C] = timer_controller.register_timer(callback_c, 100, etl::timer::mode::REPEATING);
    temperature_timer = timer_controller.register_timer(temperature_callback, 1000*60, etl::timer::mode::REPEATING);
    timer_controller.enable(true);


    Configure_ADC();
    Activate_ADC();


    loopa.InitObjects(LOOP_A, BAR_A);
    loopb.InitObjects(LOOP_B, BAR_B);
    loopc.InitObjects(LOOP_C, BAR_C);
    StartReceived();

    uint32_t bkp_val = RTC->BKP0R;
    SEGGER_RTT_printf(0, "BKP: %u\n", bkp_val);
    if ((bkp_val != 0) && (bkp_val != 1) && (bkp_val != 2) && (bkp_val != 3) && (bkp_val != 4)) {
        bkp_val = 0;
    }
    SEGGER_RTT_printf(0, "BKP Checked: %u\n", bkp_val);

    uint16_t uSettings = 0;
    while (xQueueReceive(xQueueSettings, &uSettings, portMAX_DELAY) != pdTRUE) {}

    UartPrint(TransmitBuffer, "\n\nStarting...\n");
    sprintf(PrintfBuffer, "SW: %X\n", uSettings);
    UartPrint(TransmitBuffer, PrintfBuffer);

    sprintf(PrintfBuffer, "State: %d\n", bkp_val);
    UartPrint(TransmitBuffer, PrintfBuffer);
    vTaskDelay(10);

    uint16_t flash_size = *reinterpret_cast<uint16_t *>(FLASHSIZE_BASE);
    sprintf(PrintfBuffer, "FLASH: 0x%X\n", flash_size);
    SEGGER_RTT_printf(0, "%s", PrintfBuffer);
    UartPrint(TransmitBuffer, PrintfBuffer);
    vTaskDelay(10);

    uint32_t uid0 = *reinterpret_cast<uint32_t *>(UID_BASE + 0x00);
    uint32_t uid1 = *reinterpret_cast<uint32_t *>(UID_BASE + 0x04);
    uint32_t uid2 = *reinterpret_cast<uint32_t *>(UID_BASE + 0x14);
    sprintf(PrintfBuffer, "MCU: 0x%X 0x%X 0x%X\n", uid0, uid1, uid2);
    SEGGER_RTT_printf(0, "%s", PrintfBuffer);
    UartPrint(TransmitBuffer, PrintfBuffer);

    if (bkp_val == 0) {
        UartPrint(TransmitBuffer, "CLEAR\n");
        vTaskDelay(10);
        UartPrint(TransmitBuffer, "TEST SEL_0\n");
        SEGGER_RTT_printf(0, "TEST SEL_1\n");

        CLEAR_BIT(uSettings, SETTINGS_FREQ_A_Msk);
        CLEAR_BIT(uSettings, SETTINGS_FREQ_B_Msk);
        CLEAR_BIT(uSettings, SETTINGS_FREQ_C_Msk);

    } else if (bkp_val == 1) {
        UartPrint(TransmitBuffer, "TEST SEL_1\n");
        SEGGER_RTT_printf(0, "TEST SEL_1\n");

        SET_BIT(uSettings, SETTINGS_FREQ_A_Msk);
        SET_BIT(uSettings, SETTINGS_FREQ_B_Msk);
        SET_BIT(uSettings, SETTINGS_FREQ_C_Msk);

    } else if (bkp_val == 2) {
        UartPrint(TransmitBuffer, "TEST RELAYS\n");
        SEGGER_RTT_printf(0, "Relay Test\n");

        constexpr TickType_t RelayTO = 5000;
        UartPrint(TransmitBuffer, "TEST RELAY A ASSERT\n");
        relays[LOOP_A].port->BSRR = relays[LOOP_A].pin;
        vTaskDelay(RelayTO);
        UartPrint(TransmitBuffer, "TEST RELAY A RELEASE\n");
        relays[LOOP_A].port->BRR = relays[LOOP_A].pin;
        vTaskDelay(RelayTO);

        UartPrint(TransmitBuffer, "TEST RELAY B ASSERT\n");
        relays[LOOP_B].port->BSRR = relays[LOOP_B].pin;
        vTaskDelay(RelayTO);
        UartPrint(TransmitBuffer, "TEST RELAY B RELEASE\n");
        relays[LOOP_B].port->BRR = relays[LOOP_B].pin;
        vTaskDelay(RelayTO);

        UartPrint(TransmitBuffer, "TEST RELAY C ASSERT\n");
        relays[LOOP_C].port->BSRR = relays[LOOP_C].pin;
        vTaskDelay(RelayTO);
        UartPrint(TransmitBuffer, "TEST RELAY C RELEASE\n");
        relays[LOOP_C].port->BRR = relays[LOOP_C].pin;
        vTaskDelay(RelayTO);

        UartPrint(TransmitBuffer, "TEST RELAYS COMPLETE\n");
        vTaskDelay(1000);

        bkp_val += 1;
        SEGGER_RTT_printf(0, "New BKP Relay: %u\n", bkp_val);
        RTC->BKP0R = bkp_val;
        vTaskDelay(10);
        NVIC_SystemReset();

    } else if (bkp_val == 3) {
        UartPrint(TransmitBuffer, "TEST LEDS\n");
        SEGGER_RTT_printf(0, "TEST LEDS\n");

        SetRedState(BAR_A, true);
        SetRedState(BAR_B, true);
        SetRedState(BAR_C, true);
        vTaskDelay(5000);
        SetRedState(BAR_A, false);
        SetRedState(BAR_B, false);
        SetRedState(BAR_C, false);
        vTaskDelay(100);
        SetRedState(BAR_A, true);
        vTaskDelay(500);
        SetRedState(BAR_B, true);
        vTaskDelay(500);
        SetRedState(BAR_C, true);
        vTaskDelay(500);

        constexpr TickType_t BarTO = 150;
        for (int i = 0; i < 8; i++) {
            SetGreenLevel(BAR_A, i);
            vTaskDelay(BarTO);
        }
        for (int i = 0; i < 8; i++) {
            SetGreenLevel(BAR_B, i);
            vTaskDelay(BarTO);
        }
        for (int i = 0; i < 8; i++) {
            SetGreenLevel(BAR_C, i);
            vTaskDelay(BarTO);
        }

        vTaskDelay(1000);
        for (int i = 7; i >= 0; --i) {
            SetGreenLevel(BAR_A, i);
            vTaskDelay(BarTO);
        }
        for (int i = 7; i >= 0; --i) {
            SetGreenLevel(BAR_B, i);
            vTaskDelay(BarTO);
        }
        for (int i = 7; i >= 0; --i) {
            SetGreenLevel(BAR_C, i);
            vTaskDelay(BarTO);
        }

        SetRedState(BAR_A, false);
        SetRedState(BAR_B, false);
        SetRedState(BAR_C, false);

        UartPrint(TransmitBuffer, "TEST LEDS COMPLETE\n");
        vTaskDelay(1000);

        bkp_val += 1;
        SEGGER_RTT_printf(0, "New BKP LEDS: %u\n", bkp_val);
        RTC->BKP0R = bkp_val;
        vTaskDelay(10);
        NVIC_SystemReset();

    } else if (bkp_val == 4) {
        UartPrint(TransmitBuffer, "TEST COMPLETE\n");
        vTaskDelay(5000);
        NVIC_SystemReset();
    }

    SetFrequencyRange(uSettings);

    loopa.SetSensitivity((uSettings & SETTINGS_SENS_A_Msk) >> SETTINGS_SENS_A_Pos);
    loopb.SetSensitivity((uSettings & SETTINGS_SENS_B_Msk) >> SETTINGS_SENS_B_Pos);
    loopc.SetSensitivity((uSettings & SETTINGS_SENS_C_Msk) >> SETTINGS_SENS_C_Pos);

    loopa.SetSelection((uSettings & SETTINGS_FREQ_A_Msk) >> SETTINGS_FREQ_A_Pos);
    loopb.SetSelection((uSettings & SETTINGS_FREQ_B_Msk) >> SETTINGS_FREQ_B_Pos);
    loopc.SetSelection((uSettings & SETTINGS_FREQ_C_Msk) >> SETTINGS_FREQ_C_Pos);

    sm_loop_a.process_event(StartEv{});
    sm_loop_b.process_event(StartEv{});
    sm_loop_c.process_event(StartEv{});

    Measure measure = {};
    TickType_t last_tick = xTaskGetTickCount();
    timer_controller.start(temperature_timer, true);

    for (;;) {
        if (xQueueReceive(xQueueLoopMeasure, &measure, 5) == pdTRUE) {
            switch (measure.loop) {
            case LOOP_A:
                sm_loop_a.process_event(MeasureEv{LOOP_A, measure.value, measure.multiplier});
                break;
            case LOOP_B:
                sm_loop_b.process_event(MeasureEv{LOOP_B, measure.value, measure.multiplier});
                break;
            case LOOP_C:
                sm_loop_c.process_event(MeasureEv{LOOP_C, measure.value, measure.multiplier});
                break;
            default:
                assert_param(0);
            }
        }

        if (sm_loop_a.is(boost::sml::X) && (sm_loop_b.is(boost::sml::X) && sm_loop_c.is(boost::sml::X))) {
            if (bkp_val == 0) {
                UartPrint(TransmitBuffer, "TEST SEL_0 COMPLETE\n");
            } else if (bkp_val == 1) {
                UartPrint(TransmitBuffer, "TEST SEL_1 COMPLETE\n");
            }

            vTaskDelay(5000);
            bkp_val += 1;
            SEGGER_RTT_printf(0, "New BKP: %u\n", bkp_val);
            RTC->BKP0R = bkp_val;
            vTaskDelay(10);
            NVIC_SystemReset();
        }

        TickType_t current_tick = xTaskGetTickCount();
        timer_controller.tick(current_tick - last_tick);
        last_tick = current_tick;
    }
}


void LoopController::SetFrequencyRange(uint16_t settings) {
    // SEL2
    if (settings & SETTINGS_FREQ_A_Msk) {
        SEL2_A_o_PORT->BSRR = SEL2_A_o_PIN;
    } else {
        SEL2_A_o_PORT->BRR = SEL2_A_o_PIN;
    }

    if (settings & SETTINGS_FREQ_B_Msk) {
        SEL1_B_o_PORT->BSRR = SEL1_B_o_PIN;
    } else {
        SEL1_B_o_PORT->BRR = SEL1_B_o_PIN;
    }

    if (settings & SETTINGS_FREQ_C_Msk) {
        SEL0_C_o_PORT->BSRR = SEL0_C_o_PIN;
    } else {
        SEL0_C_o_PORT->BRR = SEL0_C_o_PIN;
    }
}

#define TEST_TIMEOUT    200
void LoopController::StartReceived() {
    SetGreen(BAR_A, 0);
    SetGreen(BAR_B, 0);
    SetGreen(BAR_C, 0);
    SetRedState(BAR_A, true);
    SetRedState(BAR_B, true);
    SetRedState(BAR_C, true);
}



void LoopController::InitObjects() {
static StaticQueue_t xQueueSettingsStatic, xQueueLoopMeasureStatic;
static uint8_t ucQueueSettingsStorage[sizeof(uint16_t) * 1], ucQueueLoopMeasureStorage[sizeof(MeasureEv) * 1];

constexpr size_t LoopTaskStackSize = configMINIMAL_STACK_SIZE * 3;  // TODO При 3 возникает переполнение стека!!!!
static StackType_t xLoopTaskStack[LoopTaskStackSize];
static StaticTask_t xTCBLoopTask;

    xQueueSettings    = xQueueCreateStatic(1, sizeof(uint16_t), ucQueueSettingsStorage, &xQueueSettingsStatic);
    xQueueLoopMeasure = xQueueCreateStatic(1, sizeof(MeasureEv), ucQueueLoopMeasureStorage, &xQueueLoopMeasureStatic);
    xTaskCreateStatic(task_lc, "Loop", LoopTaskStackSize, this, tskIDLE_PRIORITY, xLoopTaskStack, &xTCBLoopTask);
}


void LoopController::UartPrint(RB &rb, const char *str) {
    xSemaphoreTake(UartMutex, portMAX_DELAY);

    size_t len = strlen(str);//, RING_BUFFER_SIZE);
    for (size_t i = 0; i < len; i++) {
        rb.Write(str[i]);
    }

    if (rb.isTransmitting) {
        xSemaphoreGive(UartMutex);
        return;
    }

    char val = 0;
    if (rb.Read(val)) {
        rb.isTransmitting = 1;
        LL_USART_TransmitData8(MODBUS_UART, val);
        LL_USART_EnableIT_TXE(MODBUS_UART);
    }
    xSemaphoreGive(UartMutex);
}


/*=================================================================================================================*/
void LoopController::TemperatureCallback() {
__IO uint16_t uhADCxConvertedData = VAR_CONVERTED_DATA_INIT_VALUE; /* ADC group regular conversion data */
__IO int16_t hADCxConvertedData_Temperature_DegreeCelsius = 0;  /* Value of temperature calculated from ADC conversion data (unit: degree Celcius) */

    ConversionStartPoll_ADC_GrpRegular();

    uhADCxConvertedData = LL_ADC_REG_ReadConversionData12(ADC1);
    hADCxConvertedData_Temperature_DegreeCelsius = __LL_ADC_CALC_TEMPERATURE(VDDA_APPLI, uhADCxConvertedData, LL_ADC_RESOLUTION_12B);
    sprintf(PrintfBuffer, "Temperature: %d C\n", hADCxConvertedData_Temperature_DegreeCelsius);
    UartPrint(TransmitBuffer, PrintfBuffer);
}


void LoopController::RedLedCallback(BarNumber bar) {
    red_state[bar] = !red_state[bar];
    SendLedUpdate();
}

void LoopController::SetRedBlink(BarNumber bar, uint32_t period) {
    auto timer_id = led_timers[bar];
    timer_controller.set_period(timer_id, period);
    timer_controller.start(timer_id, true);
}

void LoopController::SetRedState(BarNumber bar, bool state) {
    timer_controller.stop(led_timers[bar]);
    red_state[bar] = state;
    SendLedUpdate();
}

void LoopController::SetGreen(BarNumber bar, uint8_t state) {
    green_state[bar] = state & 0x7F;
    SendLedUpdate();
}

void LoopController::SetGreenLevel(BarNumber bar, uint8_t level) {
    green_state[bar] = utils::GetGreenLevel(level);
    SendLedUpdate();
}

void LoopController::SendLedUpdate() {
    if (!xLedState)
        return;

    uint32_t led_all = 0;
    uint8_t led_bar = (red_state[BAR_A] << 7) + green_state[BAR_A];
    led_all = led_bar;

    led_bar = (red_state[BAR_B] << 7) + green_state[BAR_B];
    led_all |= led_bar << 8;

    led_bar = (red_state[BAR_C] << 7) + green_state[BAR_C];
    led_all |= led_bar << 16;

    xQueueSend(xLedState, &led_all, 10);
}


/*=================================================================================================================*/


void Loop::StartReceived() const {
    SEGGER_RTT_printf(0, MODNAME"%c) Start Received\n", _name);
}



void Loop::OnCalibratingEnter() {
    SEGGER_RTT_printf(0, MODNAME"%c) Entering calibrate\n", _name);
    iCalibrationEndTime = xTaskGetTickCount() + CalibrationTime;
    iGuardCounter = 0;
    Calibrations.CalibrationValue = 0;
    adata.clear();
    sprintf(PrintfBuffer, "%c) OnCalibratingEnter\n", _name);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);
}


__attribute__((unused)) LoopError Loop::GetLoopError(float value) {
    lc.SetRedState(_bar, false);
    return LE_NO_ERROR;
}

LoopError Loop::GetLoopError(int32_t value) {
    lc.SetRedState(_bar, false);
    return LE_NO_ERROR;
}


bool Loop::CalibrationError() {
    auto err = GetLoopError(Calibrations.CalibrationValue);
    return (err == LE_SHORT) || (err == LE_OPEN);
}


void Loop::ErrorEntry() {
    // TODO Заменить 83 на нормальное значение дефайна
    iErrorTimeout = 83; // ~ 10 секунд в состоянии ошибки
}

bool Loop::ErrorGuard() {
    if (iErrorTimeout == 0)
        return true;

    iErrorTimeout--;
    return false;
}

bool Loop::CalibratingGuard(const MeasureEv &e) {
    TickType_t tick = xTaskGetTickCount();

    auto cal = filter_sample.sample(e.value);
    sprintf(PrintfBuffer, "%c)[%u] Cal M: %d, F: %d\n", _name, tick, e.value, (int)cal);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);

    if (tick > iCalibrationEndTime)
        return true;

    iGuardCounter++;
    if (iGuardCounter % 2 == 0) {
        lc.SetGreen(_bar, 0xFF);
    } else {
        lc.SetGreen(_bar, 0x00);
    }
    return false;
}


void Loop::CalibratingDone() {
    lc.SetGreen(_bar, 0);
    sprintf(PrintfBuffer, "%c) Calibration Done\n", _name);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);
    vTaskDelay(5);

    auto cal = filter_sample.value();
    Calibrations.CalcCalibration(cal, 0.0);
    char *s;
    char b[FLOAT_BUFFER_SIZE] = {};
    s = Calibrations.PrintCalibration(b);
    sprintf(PrintfBuffer, "%c) Cal value: %s\n", _name, s);
    SEGGER_RTT_printf(0, MODNAME"%s", PrintfBuffer);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);
    vTaskDelay(15);

    SEGGER_RTT_printf(0, "%c) s:%d m:%u c:%s\n", _name, bSelection, tim_multiplier, s);
    sprintf(PrintfBuffer, "%c) s:%d m:%u c:%s\n", _name, bSelection, tim_multiplier, s);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);
    vTaskDelay(5);
}


bool Loop::PreCalibratingGuard(const MeasureEv &e) {
    if (iGuardCounter > 0) {
        adata.push_back(e.value);
    }
    if (iGuardCounter == PRECALIBRATION_COUNT) {
        return true;
    }

    iGuardCounter++;
    if (iGuardCounter % 2 == 0) {
        lc.SetGreen(_bar, 0xFF);
    } else {
        lc.SetGreen(_bar, 0x00);
    }
    return false;
}


void Loop::PreCalDone() {
    sprintf(PrintfBuffer, "%c) PreCalibration Done\n", _name);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);

    // Простой фильтр, выкидываем самый старший и самый младший элементы, как самые маловероятные
    asort(adata);
    adata[0] = 0;
    adata[adata.size() - 1] = 0;

    uint32_t cal = 0;
    for (size_t i = 1; i < adata.size() - 1; i++) {
        auto el = adata[i];
        cal += el;
        sprintf(PrintfBuffer, "%c) FreqPre: %d\n", _name, el);
        lc.UartPrint(TransmitBuffer, PrintfBuffer);
        vTaskDelay(1);
    }

    cal = cal / (PRECALIBRATION_COUNT - 2);
    sprintf(PrintfBuffer, "%c) PreCal: %u\n", _name, (unsigned int)cal);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);

    Multiplier mul;
    mul.multiplier = 50000 / cal + 1;
    if (mul.multiplier > 400) {
        mul.multiplier = 400;
    }
    sprintf(PrintfBuffer, "%c) Mul: %u\n", _name, mul.multiplier);
    lc.UartPrint(TransmitBuffer, PrintfBuffer);
    tim_multiplier = mul.multiplier;

    mul.loop = _loop;
    xQueueSend(lc.xQueueMultiplier, &mul, 10);
}

bool Loop::PreCalCheck() {
    if (tim_multiplier == 1) {
        lc.SetRedState(_bar, true);
        lc.SetGreenLevel(_bar, 0);
        return true;
    }
    return true;
}

}


lc::LoopController &getLC() {
    return lc::ctrl;
}


extern "C" void USART1_IRQHandler() {
    using namespace lc;
    if (LL_USART_IsEnabledIT_TXE(MODBUS_UART) and LL_USART_IsActiveFlag_TXE(MODBUS_UART)) {
        if (TransmitBuffer.Count() == 1) {
            LL_USART_DisableIT_TXE(MODBUS_UART);
            LL_USART_EnableIT_TC(MODBUS_UART);
        }

        char val = 0;
        TransmitBuffer.Read(val);
        LL_USART_TransmitData8(MODBUS_UART, val);
    }

    if (LL_USART_IsEnabledIT_TC(MODBUS_UART) and LL_USART_IsActiveFlag_TC(MODBUS_UART)) {
        LL_USART_ClearFlag_TC(MODBUS_UART);
        LL_USART_DisableIT_TC(MODBUS_UART);
        TransmitBuffer.isTransmitting = 0;
    }

    if (LL_USART_IsEnabledIT_ERROR(MODBUS_UART) and LL_USART_IsActiveFlag_NE(MODBUS_UART)) {
        __NOP();
    }
}
