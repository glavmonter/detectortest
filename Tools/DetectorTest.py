from dataclasses import dataclass
import logging
from logging.handlers import RotatingFileHandler

import typing
from PyQt5.QtCore import QCoreApplication, QTimer, QSettings, QAbstractTableModel, Qt, QModelIndex, QVariant
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtSerialPort import QSerialPort
from MainWindow_ui import Ui_MainWindow
import re


@dataclass
class LoopData:
    loop: str


class TableModel(QAbstractTableModel):
    def __init__(self):
        super(TableModel, self).__init__()
        self._headers = ['Loop', 'Frequency Selection', 'Multiplier', 'Calibrate', 'Frequency, Hz']

        self._row_A0 = {0: 'A', 1: -1, 2: 0, 3: 0, 4: 0}
        self._row_A1 = {0: 'A', 1: -1, 2: 0, 3: 0, 4: 0}
        self._row_B0 = {0: 'B', 1: -1, 2: 0, 3: 0, 4: 0}
        self._row_B1 = {0: 'B', 1: -1, 2: 0, 3: 0, 4: 0}
        self._row_C0 = {0: 'C', 1: -1, 2: 0, 3: 0, 4: 0}
        self._row_C1 = {0: 'C', 1: -1, 2: 0, 3: 0, 4: 0}

        self._rows = {'A0': self._row_A0,
                      'A1': self._row_A1,
                      'B0': self._row_B0,
                      'B1': self._row_B1,
                      'C0': self._row_C0,
                      'C1': self._row_C1}

    def clear(self):
        self._rows['A0'] = {0: 'A', 1: -1, 2: 0, 3: 0, 4: 0}
        self._rows['A1'] = {0: 'A', 1: -1, 2: 0, 3: 0, 4: 0}
        self._rows['B0'] = {0: 'B', 1: -1, 2: 0, 3: 0, 4: 0}
        self._rows['B1'] = {0: 'B', 1: -1, 2: 0, 3: 0, 4: 0}
        self._rows['C0'] = {0: 'C', 1: -1, 2: 0, 3: 0, 4: 0}
        self._rows['C1'] = {0: 'C', 1: -1, 2: 0, 3: 0, 4: 0}

    def updateData(self, row, field, data):
        self._rows[row][field] = data

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> typing.Any:
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self._headers[section]
        return QVariant()

    def setData(self, index: QModelIndex, value: typing.Any, role: int = ...) -> bool:
        if role == Qt.DisplayRole:
            self._data[index.row()][index.column()] = value
            self.dataChanged.emit(index, index)
        return True

    def data(self, index, role):
        if role == Qt.DisplayRole:
            row = index.row()
            column = index.column()
            if row == 0:
                return self._rows['A0'][column]
            elif row == 1:
                return self._rows['B0'][column]
            elif row == 2:
                return self._rows['C0'][column]
            elif row == 3:
                return self._rows['A1'][column]
            elif row == 4:
                return self._rows['B1'][column]
            elif row == 5:
                return self._rows['C1'][column]
            else:
                return QVariant()

        return QVariant()

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return 6

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return len(self._headers)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        self.re_test_state = re.compile(r'^(TEST\s[\w\s]+)$')
        self.re_switch_state = re.compile(r'^SW:\s([0-9a-fA-F]+)$')
        self.re_temperature = re.compile(r'^Temperature:\s(-?[0-9]+)\sC$')
        self.re_flash_size = re.compile(r'^FLASH:\s0x([0-9a-fA-F]+)$')
        self.re_mcu_id = re.compile(r'^MCU:\s0x([0-9a-fA-F]+)\s0x([0-9a-fA-F]+)\s0x([0-9a-fA-F]+)$')
        self.re_loop_result = re.compile(r'^([ABC])\)\ss:([01])\sm:(\d+)\sc:(\d+)$')
        self.re_clear = re.compile(r'^CLEAR$')

        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle('Detector Testing Utility')

        self.logger_data = logging.getLogger('Data')
        self.logger = logging.getLogger('Detector')
        self.logger.debug('Create UI')
        self.tabWidget.setCurrentIndex(0)

        settings = QSettings()
        self.editPortName.setText(settings.value('port', 'COM1', type=str))
        self.spinPortBaudrate.setValue(settings.value('baudrate', 256000, type=int))
        self.editPortName.textEdited.connect(self.portNameEdited)
        self.spinPortBaudrate.valueChanged[int].connect(self.portBaudrateChanged)

        self.timerSearch = QTimer()
        self.timerSearch.timeout.connect(self.checkPortOpen)

        self.serialPort = QSerialPort()
        self.serialPort.errorOccurred.connect(self.serialPortError)
        self.serialPort.readyRead.connect(self.serialPortReadyRead)

        self.timerSearch.start(1000)
        self.rx_data = b''

        self.model = TableModel()
        self.tableResults.setModel(self.model)
        self.tableResults.resizeColumnsToContents()
        self.tableResults.resizeRowsToContents()
        self.tableResults.horizontalHeader().setStretchLastSection(True)

    def checkPortOpen(self):
        settings = QSettings()
        portname = settings.value('port', 'COM1')
        baudrate = settings.value('baudrate', 256000)
        self.logger.info('Try open port')

        if self.openPort(portname, baudrate):
            self.logger.info('Port {} opened on {} bps'.format(portname, baudrate))
            self.timerSearch.stop()
            self.labelPortStatus.setText('Port {} opened'.format(portname))

    def openPort(self, portName, baudrate):
        self.serialPort.setPortName(portName)
        self.serialPort.setBaudRate(baudrate)
        self.serialPort.setDataBits(QSerialPort.Data8)
        self.serialPort.setFlowControl(QSerialPort.NoFlowControl)
        self.serialPort.setParity(QSerialPort.NoParity)
        self.serialPort.setStopBits(QSerialPort.OneStop)
        return self.serialPort.open(QSerialPort.ReadWrite)

    def portClose(self):
        self.labelPortStatus.setText('Port closed')
        self.serialPort.close()
        self.timerSearch.start(1000)

    def serialPortError(self, error):
        if error != QSerialPort.NoError and error != QSerialPort.NotOpenError:
            self.logger.error('Serial port error: {}'.format(self.serialPort.errorString()))
            self.portClose()

    def serialPortReadyRead(self):
        d = self.serialPort.readAll()
        self.rx_data += d
        while not d.isEmpty():
            d = self.serialPort.readAll()
            self.rx_data += d

        index = self.rx_data.indexOf(b'\n')
        while index != -1:
            line = self.rx_data[:index]
            self.rx_data = self.rx_data[index + 1:]
            index = self.rx_data.indexOf(b'\n')
            try:
                line = line.data().decode('ascii')
                self.process_input(line)
            except Exception as e:
                self.logger.error(e)

    def process_input(self, line):
        self.logger_data.info(f'Data: `{line}`')

        parsed = self.re_clear.findall(line)
        if len(parsed) > 0:
            self.logger.info('Clear all!')
            self.logger_data.info('Clear all!')
            self.model.clear()
            self.tableResults.reset()

        parsed = self.re_switch_state.findall(line)
        if len(parsed) > 0:
            switch_all = int(parsed[0], 16)
            switch_sw0 = '{:08b}'.format(switch_all >> 8)
            switch_sw1 = '{:08b}'.format(switch_all & 0xFF)
            logger.info(f'Switch changed {switch_sw0} {switch_sw1}')
            self.editSW0.setText(f'Switch 0: {switch_sw0}')
            self.editSW1.setText(f'Switch 1: {switch_sw1}')

        parsed = self.re_test_state.findall(line)
        if len(parsed) > 0:
            self.editTestState.setText(f'{parsed[0]}')
            self.logger.info(f'State: {parsed[0]}')

        parsed = self.re_temperature.findall(line)
        if len(parsed) > 0:
            self.editTemperature.setText(f'Temperature: {parsed[0]} C')

        parsed = self.re_flash_size.findall(line)
        if len(parsed) > 0:
            flash_size = int(parsed[0], 16)
            self.editFlash.setText(f'Flash size: {flash_size} kB')
            self.logger.info(f'Flash size: {flash_size} kB')

        parsed = self.re_mcu_id.findall(line)
        if len(parsed) > 0:
            mcu = f'MCU: {parsed[0][0]:>08s} {parsed[0][1]:>08s} {parsed[0][2]:>08s}'
            self.editMcuID.setText(mcu)
            self.logger.info(mcu)

        parsed = self.re_loop_result.findall(line)
        if len(parsed) > 0:
            loop_name = parsed[0][0]
            loop_sel = int(parsed[0][1])
            loop_mul = int(parsed[0][2])
            loop_calibrate = int(parsed[0][3])
            freq = self.get_freq(loop_mul, loop_calibrate)
            loop_key = f'{loop_name}{loop_sel}'
            sel = '0 Low' if loop_sel == 0 else '1 High'
            self.model.updateData(loop_key, 1, sel)
            self.model.updateData(loop_key, 2, loop_mul)
            self.model.updateData(loop_key, 3, loop_calibrate)
            self.model.updateData(loop_key, 4, freq)
            self.tableResults.reset()
            self.logger.info(f'Loop {loop_name}, sel: {sel}, mul: {loop_mul}, cal: {loop_calibrate}, freq: {freq}')

    def get_freq(self, mul, cal):
        t = 1/32e6
        try:
            f = 1/(t * cal / mul)
        except ZeroDivisionError:
            f = 0
        return f

    def calculate_freq(self, loop_dict):  # TODO Пересчет в частоту
        t = 1/32e6
        try:
            f = 1/(t * loop_dict['freq']['value']/loop_dict['freq']['mul'])
        except ZeroDivisionError:
            f = 0
        widget = loop_dict['mul']
        text = 'Mul: {}, Freq: {:0.1f} Hz'.format(loop_dict['freq']['mul'], f)
        widget.setText(text)

    def update_log(self, log_struct):
        log_struct['widget'].clear()
        for line in log_struct['last']:
            log_struct['widget'].appendPlainText(line)

    def portNameEdited(self, text):
        self.portClose()
        settings = QSettings()
        settings.setValue('port', text)

    def portBaudrateChanged(self, value):
        self.portClose()
        settings = QSettings()
        settings.setValue('baudrate', value)


if __name__ == '__main__':
    import sys
    import os
    if not os.path.exists('logs'):
        os.mkdir('logs')

    file_handler = RotatingFileHandler('logs/DetectorTest.log', maxBytes=1024*1024*20, backupCount=50)
    file_handler.setFormatter(
        logging.Formatter('[%(name)-12s] [%(levelname)-8s] [%(asctime)s] %(message)s', datefmt='%Y-%m-%d, %H:%M:%S'))
    file_handler.setLevel(logging.DEBUG)

    file_handler_data = RotatingFileHandler('logs/DetectorTestData.log', maxBytes=1024 * 1024 * 20, backupCount=50)
    file_handler_data.setFormatter(
        logging.Formatter('[%(name)-12s] [%(levelname)-8s] [%(asctime)s] %(message)s', datefmt='%Y-%m-%d, %H:%M:%S'))
    file_handler_data.setLevel(logging.DEBUG)

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(
        logging.Formatter('[%(name)-12s] [%(levelname)-8s] [%(asctime)s] %(message)s', datefmt='%Y-%m-%d, %H:%M:%S'))

    logger = logging.getLogger('Detector')
    logger.addHandler(console)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)
    logger.debug('Started')

    logger_data = logging.getLogger('Data')
    logger_data.addHandler(file_handler_data)
    logger_data.setLevel(logging.INFO)

    QCoreApplication.setOrganizationName('RPS')
    QCoreApplication.setApplicationName('DetectorTest')

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    ret = app.exec_()
    logger.info('Stopped')
    sys.exit(ret)
